package de.pandaswelt.mysecureapplication.ui.main

import android.content.Context
import android.content.SharedPreferences
import android.os.Environment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import java.io.File

class MainViewModel : ViewModel() {
    val contentLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    val backgroundColorLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun executeUiAction(uiAction: UiAction) {
        when (uiAction) {
            is UiAction.ReadFile -> {
                readFromSecureFile(uiAction.context)
            }
            is UiAction.WriteFile -> {
                writeToSecureFile(uiAction.context, uiAction.content)
            }
            is UiAction.ReadColor -> {
                readPreferences(uiAction.context)
            }
            is UiAction.WriteColor -> {
                writePreferences(uiAction.context, uiAction.color)
            }
        }
    }

    // Private execution methods

    private val keyGenParameterSpec = MasterKeys.AES256_GCM_SPEC
    private val masterKeyAlias = MasterKeys.getOrCreate(keyGenParameterSpec)

    private fun writeToSecureFile(context: Context, content: String) {
        val file = File(context.getExternalFilesDir(null), FILENAME)
        // clean up before
        if(file.exists()){
            file.delete()
        }

        val encryptedFile = EncryptedFile.Builder(
                file,
                context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        encryptedFile.openFileOutput().bufferedWriter().use {
            it.write(content)
        }
    }

    private fun readFromSecureFile(context: Context) {
        val file = File(context.getExternalFilesDir(null), FILENAME)
        val encryptedFile = EncryptedFile.Builder(
                file,
                context,
                masterKeyAlias,
                EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()

        val contents = encryptedFile.openFileInput().bufferedReader().useLines { lines ->
            lines.fold("") { working, line ->
                "$working\n$line"
            }
        }
        contentLiveData.postValue(contents)
    }

    private fun readPreferences(context: Context) {
        val preferences = getPreferences(context)
        backgroundColorLiveData.postValue(
                preferences.getString(PREFERENCE_BACKGROUND_COLOR_KEY, "#ffffff")
        )
    }

    private fun writePreferences(context: Context, colorString: String) {
        val preferences = getPreferences(context)
        preferences.edit().putString(
                PREFERENCE_BACKGROUND_COLOR_KEY,
                colorString
        ).apply()
    }

    private fun getPreferences(context: Context): SharedPreferences {
        return EncryptedSharedPreferences
                .create(
                        FILENAME_PREFERENCES,
                        masterKeyAlias,
                        context,
                        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
    }

    companion object {
        const val FILENAME = "my_sensitive_data.txt"
        const val FILENAME_PREFERENCES = "app_preferences"
        const val PREFERENCE_BACKGROUND_COLOR_KEY = "background_color"
    }
}