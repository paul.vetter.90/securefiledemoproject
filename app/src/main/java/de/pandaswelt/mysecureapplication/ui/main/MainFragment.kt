package de.pandaswelt.mysecureapplication.ui.main

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.pandaswelt.mysecureapplication.R
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        viewModel.backgroundColorLiveData.observe(viewLifecycleOwner, Observer {
            main.setBackgroundColor(
                    Color.parseColor(it)
            )
        })
        viewModel.contentLiveData.observe(viewLifecycleOwner, Observer {
            file_content_text.text = it
        })

        file_content_save.setOnClickListener {
            viewModel.executeUiAction(UiAction.WriteFile(it.context, file_content_edit.text.toString()))
        }

        file_content_load.setOnClickListener {
            viewModel.executeUiAction(UiAction.ReadFile(it.context))
        }

        color_save.setOnClickListener {
            viewModel.executeUiAction(UiAction.WriteColor(it.context, color_edit.text.toString()))
        }

        color_load.setOnClickListener {
            viewModel.executeUiAction(UiAction.ReadColor(it.context))
        }
    }

}