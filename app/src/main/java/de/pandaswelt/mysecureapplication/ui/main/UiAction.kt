package de.pandaswelt.mysecureapplication.ui.main

import android.content.Context

sealed class UiAction(val context: Context){
    class ReadFile(context: Context): UiAction(context)
    class WriteFile(context: Context, val content: String) : UiAction(context)
    class ReadColor(context: Context): UiAction(context)
    class WriteColor(context: Context, val color: String) : UiAction(context)
}